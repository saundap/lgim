package com.legalandgeneral.dataprocessors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class FileManager {
    String input_path;
    Path banned_list;
    boolean parallel;

    public void readFile(List<String> inputDataFiles, String input_path, Path banned_list, boolean parallel) {

        if (Objects.isNull(input_path) || StringUtils.isEmpty(input_path)) {
            throw new IllegalArgumentException(
                    "The provided Input Data File Path argument was null or empty; non-null value must be provided.");
        }

        if (Objects.isNull(banned_list)) {
            throw new NullPointerException(
                    "The provided Banned List argument was null; non-null value must be provided.");
        }

        if (Objects.isNull(inputDataFiles)) {
            throw new NullPointerException(
                    "The provided List of Input Data Files argument was null; non-null value must be provided.");
        }


        this.input_path = input_path;
        this.parallel = parallel;
        this.banned_list = banned_list;

        if (inputDataFiles.size()==0) {

            streamFiles();

        } else {

            for (String inputDataFile : inputDataFiles) {
                streamStringData(inputDataFile);
            }

        }
    }

    private void streamStringData(String inputDataFile){

            try (Stream<String> stream = Files.lines(Paths.get(input_path, inputDataFile))) {

                if(parallel){
                    stream.parallel();
                }
                List<String> filteredLines = stream
                        .map(line -> {
                            try {
                                return FilterBannedWords.censor(line, formRegexPattern(banned_list));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return line;
                        })
                        .collect(Collectors.toList());

                filteredLines.stream().distinct().forEach(line -> printLimit(line, 30));

            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private void streamFiles(){
        try (Stream<Path> paths = Files.walk(Paths.get(input_path))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(inputDataFile -> streamStringData(inputDataFile.getFileName().toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printLimit(Object obj, int width) {

        String val = obj.toString();

        if (val.length() > width && width > 0) {
            System.out.println(val.substring(0, width));
            printLimit(val.substring(width, val.length()), width);
        } else {
            System.out.println(val);
        }

    }

    public String formRegexPattern(Path list) throws IOException {

        if(StringUtils.isEmpty(list.toString())){
            throw new IllegalArgumentException(
                    "The provided list was null; non-null value must be provided.");
        }

        return Files.lines(list)
                .map( n -> "\\b"+n.toString()+"\\b" )
                .collect(Collectors.joining("|"))+"?";

    }

    public JSONObject getJSONConfigs(String json_config_path) throws FileNotFoundException {
        JSONParser parser = new JSONParser();
        new JSONObject();
        JSONObject jsonObject;
            try {
                Object obj = parser.parse(new FileReader(json_config_path));
                jsonObject = (JSONObject)obj;
            } catch(IOException | ParseException e) {
                throw new FileNotFoundException(
                        "The JSON Configuration File was not found");
            }
        return jsonObject;
    }

    public void validateExtension(String file_name, String expected_extension){
        if (!(expected_extension).toLowerCase().equals(FilenameUtils.getExtension(file_name)))
        {
            throw new IllegalArgumentException(
                    "The provided "+file_name+" must be of file type .txt");

        }
    }
}
