package com.legalandgeneral.dataprocessors;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.regex.Pattern;

public class FilterBannedWords {

    // Function takes two parameter
    public static String censor(String textLine,  String banned_word_list) {
        String[] line = textLine.split("\\s+");
        Pattern pattern = Pattern.compile(banned_word_list, Pattern.CASE_INSENSITIVE);

            for (int index = 0; index < line.length; index++) {
                if(line[index].trim().length()==0){
                    continue;
                }
                    if (matches(line[index], pattern)) {

                        // changing the censored word to
                        // created asterisks censor
                        line[index] = RegExUtils.replaceAll(line[index].toLowerCase(), pattern, "*".repeat(line[index].length()));
                    }
            }

        StringBuilder result = new StringBuilder();
            for (String y : line)
                result.append(y).append(' ');

            return result.toString();
        }

    public static Boolean matches(String word, Pattern pattern){
        if(Objects.isNull(word) || StringUtils.isEmpty(word)){
                throw new IllegalArgumentException(
                        "The provided banned word was null; non-null value must be provided.");
        }
        return pattern.matcher(word.toLowerCase()).find();
    }



}
