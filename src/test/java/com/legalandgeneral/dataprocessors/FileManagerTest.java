package com.legalandgeneral.dataprocessors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class FileManagerTest {

    FileManager fm = new FileManager();

    @Test
    public void fileInputPath_null_illegal_argument_thrown(){
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> fm.readFile(Arrays.asList("test.txt"),
                        null,
                        Paths.get("src/main/resources/", "BannedWordListShort.txt"),
                        true),
                "The provided Input Data Files list argument was null or empty; non-null value must be provided."
        );

        Assertions.assertTrue(thrown.getMessage().contains("Input Data"));
    }

    @Test
    public void fileInputPath_empty_string_illegal_argument_thrown(){
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> fm.readFile(Arrays.asList("test.txt"),
                        "",
                        Paths.get("src/test/resources/", "BannedWordListShort.txt"),
                        true),
                "The provided Input Data Files list argument was null or empty; non-null value must be provided."
        );

        Assertions.assertTrue(thrown.getMessage().contains("Input Data"));
    }

    @Test
    public void banned_list_null_pointer_thrown(){
        NullPointerException thrown = Assertions.assertThrows(
                NullPointerException.class,
                () -> fm.readFile(Arrays.asList("test.txt"),
                        "src/test/resources/",
                        null,
                        true),
                "\"The provided Banned List argument was null or empty; non-null value must be provided.\""
        );

        Assertions.assertTrue(thrown.getMessage().contains("Banned List"));
    }

    @Test
    public void input_data_file_list_null_pointer_thrown(){
        NullPointerException thrown = Assertions.assertThrows(
                NullPointerException.class,
                () -> fm.readFile(null,
                        "src/test/resources/",
                        Paths.get("src/test/resources/"),
                        true)
        );

        Assertions.assertTrue(thrown.getMessage().contains("Input Data Files"));
    }

    @Test
    public void empty_input_data_file_list_no_exception_thrown(){
        Assertions.assertDoesNotThrow(
                () ->  fm.readFile(new ArrayList(),
                        "src/test/resources/",
                        Paths.get("banned_list_test.txt"),
                        true)
        );
    }


    @Test
    public void json_file_not_found_exception_thrown() throws FileNotFoundException {
        FileNotFoundException thrown = Assertions.assertThrows(
                FileNotFoundException.class,
                    () ->  fm.getJSONConfigs("src/test/resources/json.config")
        );

        Assertions.assertTrue(thrown.getMessage().contains("JSON Configuration File was not found"));
    }

    @Test
    public void extension_check_throws_exception() throws FileNotFoundException {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () ->  fm.validateExtension("test.txt", "exe")
        );

        Assertions.assertTrue(thrown.getMessage().contains("type .txt"));
    }

    @Test
    public void extension_check_does_not_throw_exception() throws FileNotFoundException {
        Assertions.assertDoesNotThrow(
                () ->  fm.validateExtension("test.txt", "txt")
        );
    }
}
