package com.legalandgeneral.dataprocessors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilterBannedWordsTest {

    @Test
    public void censorBannedWords_multipleMatches() {
        Assertions.assertEquals("**** ** * **** ", FilterBannedWords.censor("This is a test", concatenateStrings(Arrays.asList("This", "is", "a", "test"))));
    }

    @Test
    public void censorBannedWords_trailingPunctuation() {
        Assertions.assertEquals("This is a *****. ", FilterBannedWords.censor("This is a test.", concatenateStrings(Arrays.asList("test"))));
    }

    @Test
    public void censorBannedWords_leadingPunctuation() {
        Assertions.assertEquals("This !*** a test. ", FilterBannedWords.censor("This !is a test.", concatenateStrings(Arrays.asList("is"))));
    }

    @Test
    public void censorBannedWords_CamelCase() {
        Assertions.assertEquals("********* is a test. ", FilterBannedWords.censor("CamelCase is a test.", concatenateStrings(Arrays.asList("CamelCase"))));
    }

    @Test
    public void censorBannedWords_UpperCase() {
        Assertions.assertEquals("********* is a test. ", FilterBannedWords.censor("lowercase is a test.", concatenateStrings(Arrays.asList("lowercase"))));
    }

    @Test
    public void censorBannedWords_LowerCase() {
        Assertions.assertEquals("********* is a test. ", FilterBannedWords.censor("UPPERCASE is a test.", concatenateStrings(Arrays.asList("UPPERCASE"))));
    }

    @Test
    public void censorBannedWords_IgnoreInnerMatch() {
        Assertions.assertEquals("InnerINNERWORKDWord ** * *****. ", FilterBannedWords.censor("InnerINNERWORKDWord is a test.", concatenateStrings(Arrays.asList("INNERWORD", "is", "a", "test"))));
    }


    @Test
    public void matching_does_match() {
        Pattern pattern = Pattern.compile("\\btest\\b", Pattern.CASE_INSENSITIVE);
        Assertions.assertTrue(FilterBannedWords.matches("test", pattern));
    }

    @Test
    public void matching_does_not_match() {
        Pattern pattern = Pattern.compile("\\btest\\b", Pattern.CASE_INSENSITIVE);
        Assertions.assertFalse(FilterBannedWords.matches("toast", pattern));
    }

    @Test
    public void matching_does_not_match_null() {
        Pattern pattern = Pattern.compile("\\btest\\b", Pattern.CASE_INSENSITIVE);
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> FilterBannedWords.matches(null, pattern)
        );

        Assertions.assertTrue(thrown.getMessage().contains("The provided banned word was null; non-null value must be provided."));
    }


    private String concatenateStrings(List<String> list){
            return list
                    .stream()
                    .map( n -> "\\b"+n.toString()+"\\b" )
                    .collect(Collectors.joining("|"));
        }

}
