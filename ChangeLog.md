# CHANGELOG.md

## 1.0.1 (04-08-2020)

Features:

  - obfuscate identified words in source file if word is matched
  - output result of altered source text to stdout
  - parallel and sequential processing toggle
  - automated tests (unit)
  - can process single input file or a directory of input files
  - all files referenced must end in .txt
  - run controlled by external configuration values in json file format
