# Banned Words Obfuscation

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Intellij Community/Ultimate 2020.2
Java openJDK 1.11.05
```

### Installing

A step by step series of examples that tell you how to get a development env running

Mac
```
brew tap AdoptOpenJDK/openjdk
brew install adoptopenjdk-openjdk11
export JAVA_HOME=`<path to openjdk11>`
```

Windows
```
https://jdk.java.net/java-se-ri/11
setx JAVA_HOME "<jdk install path>"
setx PATH "%PATH%;%JAVA_HOME%\bin";
```

Intellij
```
File -> Project Structure -> SDKs -> set to Java 11.05 via d/l or add if d/l already
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests
Mac/*nix
```
./gradlew test
```
Windows
```
gradlew test
```

## Deployment

Add additional notes about how to deploy this on a live system


##Running

* Utilises a Json configuration file for inputs and processing rules;
* Properties are NOT optional;
* If the target file name property is an empty array, then all files at target path will process;
* Run with parallel==true for Stream parallelism - consumes much more CPU;
* Java Args accepted is path to a json configuration file containing:
```
{  
  "target_file_names": <Array of Strings>,
  "target_file_path": <String>,
  "banned_file_name": <String>,
  "ban_file_path": <String>,
  "parallel": <boolean>
}
```
* For input files, extension '.txt' is strict


####Within IDE using gradle wrapper

```
./gradlew <command> [options]
Options:
  --args            
      --json config path    [optional] path to json configurations
 e.g.
./gradlew dataObfuscation --args="src/main/resources/configurations.json"
```
* Options are space separated string array of file names located under main/src/resources by default
* Properties are preceded with '-P'

####Generate Jar and Deploy

```
from project root
    ./gradlew clean
    ./gradlew build
    cd build/libs/
```
Deploy jar named LGIMBannedWords-<verison>.jar

Target files for processing and banned word list may be located at any permitted directory level on deployment server. 

sample run command to use packaged configurations.json

```
 java -jar LGIMBannedWords-1.0.1.jar "src/main/resources/configurations.json"
```
sample run command to utilise external to package configurations.json
```
 java -jar LGIMBannedWords-1.0.1.jar "/tmp/configurations.json"
```

## Built With

* [Gradle 5.6.4](https://docs.gradle.org/5.6.4/userguide/userguide.html) - Dependency Management
* [Java openJDK 11.05](https://adoptopenjdk.net/release_notes.html#jdk11_0_5) - Dependency Management
* [Apache Commons IO](https://commons.apache.org/io/) - File IO Controls
* [Apache Commons Lang3](https://commons.apache.org/proper/commons-lang/download_lang.cgi) - String Utilities and Regex
* [Junit](https://junit.org/junit5/docs/current/user-guide/) - Unit Test Framework
* [Json Simple](https://code.google.com/archive/p/json-simple/) - Google JsonSimple Library

## Authors

* **Andrew Saunders** - *Initial work* - [GitLab](https://gitlab.com/saundap)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
